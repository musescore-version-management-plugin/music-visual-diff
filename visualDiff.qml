import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import MuseScore 3.0

MuseScore {
      menuPath: "Plugins.Viasual Difference"
      description: qsTr("Visual reprezentation between two different score with colored notes. Made by Tamás Sebestyén")
      version: "1.0"
      requiresScore: true
      pluginType: "dock"
      dockArea: "left"

      ListModel {id: modificationsModel}
      
      Text {
            id: modificationsViewLabel
            visible: false
            text: qsTr("Modifications:")
            x: 10
            y: 70
      }
      
      TreeView {
            TableViewColumn {
                  title: qsTr("Old measure")
                  role: "s1m"
                  width: 100
            }
            TableViewColumn {
                  title: qsTr("Modified measure")
                  role: "s2m"
                  width: 100
            }
            model: modificationsModel
            id: modificationsView
            visible: false
            width: 202
            x: 10
            y: 90
      }
      
      Button {
            id: bCompare
            text: qsTr("Compare")
            x: 10
            y: 10
            onClicked: {
                  compareScores(scores[0], scores[1])
            }
      }
      
      Button {
            id: bColorBlack
            text: qsTr("Uncompare");
            x: 10
            y: 40
            onClicked: {
                  colorScoresBackToBlack(scores[0], scores[1]);
                  modificationsModel.clear();
                  modificationsViewLabel.visible = false;
                  modificationsView.visible = false;
            }
      }
      
      function colorScoresBackToBlack(score1, score2) {
            var cursor1 = score1.newCursor();
            var cursor2 = score2.newCursor();
            cursor1.rewind(Cursor.SCORE_START);
            cursor2.rewind(Cursor.SCORE_START);
            var colorItBackToBlack = function(score, cursor) {
                  for(var i=0; i<score.ntracks; i++) {
                        for(var j=0; j<score.nmeasures; j++) {
                              var segment = cursor.measure.firstSegment;  
                              while(segment) {
                                    var track = i*4;
                                    var element = segment.elementAt(track);
                                    if(element == null) break;
                                    coloring(element, "black");
                                    segment = segment.nextInMeasure;
                              }
                              cursor.nextMeasure();
                        }
                        cursor.rewind(Cursor.SCORE_START);
                  }
            }
            colorItBackToBlack(score1, cursor1);
            colorItBackToBlack(score2, cursor2);
      }
      
      function initializeParts(score1, score2, partsArray) {
            var maxNumOfParts = Math.max(score1.parts.length, score2.parts.length);
            var testScore1Parts = function(number) {
                  var itIsNew = true;
                  for(var i = 0; i<partsArray.length; i++) {
                        if(partsArray[i].oldScore == number) itIsNew = false;
                  }
                  return itIsNew;      
            }
            var testScore2Parts = function(number) {
                  var itIsNew = true;
                  for(var i = 0; i<partsArray.length; i++) {
                        if(partsArray[i].newScore == number) itIsNew = false;
                  }
                  return itIsNew;
            }
            
            for(var i=0; i<=maxNumOfParts; i++) {
                  if(score1.parts[i] == null) {
                        for(var j=0; j<=maxNumOfParts; j++) {
                              if(score2.parts[j] == null) break;
                              else {
                                    if(testScore2Parts(j)) {
                                          partsArray.push({oldScore: null, newScore: j, comparisonMatrix: null}); 
                                    }
                              }
                        }
                  }
                  else {                  
                        for(var j=0; j<=maxNumOfParts; j++) {
                              if(score2.parts[j] == null) {
                                    if(testScore1Parts(i)) {
                                          partsArray.push({oldScore: i, newScore: null, comparisonMatrix: null});
                                    }
                              }     
                              else {
                                    if(score1.parts[i].partName == score2.parts[j].partName) {
                                          if(testScore1Parts(i) && testScore2Parts(j)) {
                                                partsArray.push({oldScore: i, newScore: j, comparisonMatrix: null});
                                          }
                                    }     
                              }
                        }   
                  }            
            } 
      }
      
      function testSameType(element1, element2) { return element1.type !== element2.type; }
      function testRestAndSameDuration(element1, element2) { return element1.type == Element.REST && testSameType(element1, element2) && element1.durationType !== element2.durationType; }
      function testSameDuration(element1, element2) { return element1.durationType !== element2.durationType; }
      function testSameNumOfNotes(element1, element2) { return element1.notes.length !== element2.notes.length; }
      function testClef(element) { return element.type == Element.CLEF; }            
      function testKeySig(element) { return element.type == Element.KEYSIG; }
      function testTimeSig(element) { return element.type == Element.TIMESIG; }
      function testEndBarLine(segment) { return segment.segmentType == Segment.EndBarLine; }
      function testBeginBarLine(segment) { return segment.segmentType == Segment.BeginBarLine; }
      function testKeySigAnnounce(segment) { return segment.segmentType == Segment.KeySigAnnounce }
      function testTimeSigAnnounce(segment) { return segment.segmentType == Segment.TimeSigAnnounce }
      function testAmbitus(segment) { return segment.segmentType == Segment.Ambitus }
      function testBreath(segment) { return segment.segmentType == Segment.Breath }
      
      function compareMeasures(measure1, measure2, track1, track2) {           
            var testSamePitch= function(element1, element2) {
                  var pitchNotEquals = 0;
                  var notes1 = element1.notes;
                  var notes2 = element2.notes;
                  var maxLength = Math.max(element1.notes.length,element2.notes.length)
                  for (var k = 0; k < maxLength; k++) {
                        if(notes1[k] == undefined || notes2[k] == undefined || notes1[k].pitch !== notes2[k].pitch) pitchNotEquals++;
                        else bestParity++; 
                  }
                  if(pitchNotEquals == 0) return false;
                  else return true;
            }
            
            var segment1 = measure1.firstSegment;
            var segment2 = measure2.firstSegment;       
            
            var m1cmp = 0;
            var m2cmp = 0;
            var bestParity = 2; 
            
            while(segment1 && segment2) {
                  if(testBeginBarLine(segment1)) segment1 = segment1.nextInMeasure;      
                  if(testBeginBarLine(segment2)) segment2 = segment2.nextInMeasure;
                  if(testAmbitus(segment1)) segment1 = segment1.nextInMeasure;      
                  if(testAmbitus(segment2)) segment2 = segment2.nextInMeasure; 
                  if(testBreath(segment1)) segment1 = segment1.nextInMeasure;      
                  if(testBreath(segment2)) segment2 = segment2.nextInMeasure; 
                  if(testKeySigAnnounce(segment1)) segment1 = segment1.nextInMeasure;
                  if(testKeySigAnnounce(segment2)) segment2 = segment2.nextInMeasure;
                  if(testTimeSigAnnounce(segment1)) segment1 = segment1.nextInMeasure;
                  if(testTimeSigAnnounce(segment2)) segment2 = segment2.nextInMeasure;   
                  
                  var element1 = segment1.elementAt(track1);
                  var element2 = segment2.elementAt(track2); 
            
                  if(element1 == null && element2 == null) break;
                  
                  if(element1 == null) {
                        if(m1cmp == m2cmp) m2cmp++;
                        break;
                  }
                  if(element2 == null) {
                        if(m1cmp == m2cmp) m1cmp++;
                        break;
                  }       
                  
                  if(testEndBarLine(segment1) && testEndBarLine(segment2)) break;
                  
                  if(testEndBarLine(segment1)) {
                        if(m1cmp == m2cmp) m2cmp++;
                        break;
                  }
                  
                  if(testEndBarLine(segment2)) {
                        if(m1cmp == m2cmp) m1cmp++;
                        break;
                  }           
                  
                  if(testClef(element1)) {
                        segment1 = segment1.nextInMeasure;
                        element1 = segment1.elementAt(track1);
                  }
                  
                  if(testClef(element2)) {
                        segment2 = segment2.nextInMeasure;
                        element2 = segment2.elementAt(track2);
                  }
                  
                  if(testKeySig(element1)) {
                        m2cmp++;
                        segment1 = segment1.nextInMeasure;
                        element1 = segment1.elementAt(track1);
                  }
                  
                  if(testKeySig(element2)) {
                        m1cmp++;
                        segment2 = segment2.nextInMeasure;
                        element2 = segment2.elementAt(track2);
                  }
                  
                  if(testTimeSig(element1)) {
                        segment1 = segment1.nextInMeasure;
                        element1 = segment1.elementAt(track1);
                  }
                  
                  if(testTimeSig(element2)) {
                        segment2 = segment2.nextInMeasure;
                        element2 = segment2.elementAt(track2);
                  } 
                  
                  if(element1.type == Element.BAR_LINE || element2.type == Element.BAR_LINE) {
                        if(element1.barlineType == 4) {
                              segment1 = segment1.nextInMeasure;
                              element1 = segment1.elementAt(track1);
                        }
                        if(element2.barlineType == 4) {
                              segment2 = segment2.nextInMeasure;
                              element2 = segment2.elementAt(track2);     
                        }
                  }
                  
                  if(element1.barlineType == 1 && element2.barlineType == 1) {
                        segment1 = segment1.nextInMeasure;
                        segment2 = segment2.nextInMeasure;
                        continue;      
                  }
                  
                  m1cmp++;                 

                  if(testSameType(element1, element2)) {
                        segment1 = segment1.nextInMeasure;
                        segment2 = segment2.nextInMeasure;
                        continue; 
                  }
            
                  if(testRestAndSameDuration(element1, element2)) {
                        segment1 = segment1.nextInMeasure;
                        segment2 = segment2.nextInMeasure;
                        continue; 
                  }                
                  
                  if(element1.type == Element.CHORD && element2.type == Element.CHORD) {
                        var chordNotEquals = 0;
                        var numberOfNotesIsNotEnough = false;
                        var durationIsNotEnough = false;
                        if(testSameDuration(element1, element2)) chordNotEquals++;
                        else {
                              bestParity++;
                              durationIsNotEnough = true;
                        }
                        if(testSameNumOfNotes(element1, element2)) chordNotEquals++; 
                        else {
                              bestParity++;                      
                              numberOfNotesIsNotEnough = true;
                        }
                        if(testSamePitch(element1, element2)) chordNotEquals++;    
                        if(chordNotEquals==2) {
                              if(numberOfNotesIsNotEnough || durationIsNotEnough) {
                                    bestParity--;
                                    segment1 = segment1.nextInMeasure;
                                    segment2 = segment2.nextInMeasure;
                                    continue;
                              }
                        }
                        if(chordNotEquals==3) {
                              bestParity = 2;
                              segment1 = segment1.nextInMeasure;
                              segment2 = segment2.nextInMeasure;
                              continue; 
                        }
                        if(chordNotEquals<3 && chordNotEquals != 0) {
                              m1cmp++;
                        }
                  }
                  m2cmp++;
                  segment1 = segment1.nextInMeasure;
                  segment2 = segment2.nextInMeasure;
            } 
            if(m1cmp==m2cmp) return 1;
            if(m1cmp>m2cmp || m1cmp<m2cmp) {
                  if(m2cmp==0) return 0;
                  else return bestParity;
            }      
      }
      
      function adjustComparisonMatrix(comparison) {
            var numOfColumns = comparison[0].length;
            var numOfRows = comparison.length;
            
            var repairRow = function(comparison, i, j) {
                  for(var k=1;k<numOfColumns;k++) {
                        if(k!==j) comparison[i][k] = 0;       
                  }
            }
            
            var repairColumn = function(comparison, i, j, modification) {
                  for(var k=1;k<numOfRows;k++) {
                        if(k!==i) comparison[k][j] = 0;
                        if(k==i && modification) comparison[k][j] = 2;
                  }
            }
            
            var tmp = 1;
            for(var i=1;i<numOfRows;i++) {
                  for(var j=tmp;j<numOfColumns;j++) {
                        if(comparison[i][j]==1) {
                              repairRow(comparison, i, j);
                              tmp = j+1;
                              break;
                        }
                  }
            }
            
            for(var i=1;i<numOfColumns;i++) {
                  for(var j=1;j<numOfRows;j++) {
                        if(comparison[j][i]==1) {
                              repairColumn(comparison, j, i, false);
                              break;
                        }
                  }
            }

            for(var i=1;i<numOfRows;i++) {
                  var tmp = 1;
                  var tmpColumn = 0;
                  for(var j=1;j<numOfColumns;j++) {
                        if(comparison[i][j]>tmp) {
                              tmpColumn = j;
                              tmp = comparison[i][j];
                        }
                  }
                  if(tmpColumn !== 0) repairRow(comparison, i, tmpColumn);
            }
            
            for(var i=1;i<numOfColumns;i++) {
                  var tmp = 1;
                  var tmpRow = 0;
                  for(var j=1;j<numOfRows;j++) {
                        if(comparison[j][i]>tmp) {
                              tmpRow = j;
                              tmp = comparison[j][i];
                        }
                  }
                  if(tmpRow !== 0) repairColumn(comparison, tmpRow, i, true);
            }
            
            return comparison;
      }
      
      function initializeComparisonMatrix(score1, score2, partsArray) {
            for(var k=0;k<partsArray.length; k++) {
                  var s1l;                      //Score1 length
                  var s2l;                      //Score2 length
                  var s1nt;                     //Score1 number of tracks
                  var s2nt;                     //Score2 number of tracks
                  var s1nm = score1.nmeasures;  //Score1 number of measures
                  var s2nm = score2.nmeasures;  //Score2 number of measures
                  var staff1 = partsArray[k].oldScore;
                  var staff2 = partsArray[k].newScore;
                  if(staff1 == null) {
                        s1l = s1nm;
                        s2nt = (score2.parts[staff2].endTrack-score2.parts[staff2].startTrack)/4;
                        s2l = s2nm*s2nt;
                  }
                  else if(staff2 == null) {
                        s1nt = (score1.parts[staff1].endTrack-score1.parts[staff1].startTrack)/4;
                        s1l = s1nm*s1nt;
                        s2l = s2nm;
                  }
                  else {  
                        s1nt = (score1.parts[staff1].endTrack-score1.parts[staff1].startTrack)/4;
                        s2nt = (score2.parts[staff2].endTrack-score2.parts[staff2].startTrack)/4;
                        s1l = s1nm*s1nt;
                        s2l = s2nm*s2nt;
                  }
                  
                  var comparison = new Array(s2l+1);
                  for(var i=0;i<=s2l;i++) comparison[i] = new Array(s1l+1)
                  for(var i=0; i<=s2l; i++) {
                        for(var j=0; j<=s1l; j++) {
                              comparison[i][j] = 0;      
                        }
                  }
                  if(partsArray[k].oldScore !== null && partsArray[k].newScore !== null) {
                        var cursor1 = score1.newCursor();
                        var cursor2 = score2.newCursor();
                        cursor1.track = score1.parts[staff1].startTrack;
                        cursor2.track = score2.parts[staff2].startTrack;
                        cursor1.rewind(Cursor.SCORE_START);
                        cursor2.rewind(Cursor.SCORE_START);
                        for(var m=1; m<=s2nt; m++) {
                              for(var n=1; n<=s1nt; n++) {
                                    if(m==n) {
                                          for(var i=m*s2nm-s2nm+1; i<=m*s2nm; i++) {
                                                for(var j=n*s1nm-s1nm+1; j<=n*s1nm; j++) {
                                                      comparison[i][j] = compareMeasures(cursor1.measure, cursor2.measure, cursor1.track, cursor2.track);
                                                      cursor1.nextMeasure();
                                                }
                                                cursor1.rewind(Cursor.SCORE_START);
                                                cursor2.nextMeasure();
                                          }
                                          cursor1.rewind(Cursor.SCORE_START);
                                          cursor2.rewind(Cursor.SCORE_START);
                                    }
                              }
                              cursor1.track += 4;
                              cursor2.track += 4;
                        }                  
                        comparison = adjustComparisonMatrix(comparison);
                  }
                  partsArray[k].comparisonMatrix = comparison;
            }
      }
      
      function coloring(element, coloring) {
            if(element.type == Element.CHORD) {
                  var notes = element.notes;
                  for(var i=0; i<notes.length; i++) {
                        notes[i].color = coloring;
                  }
            }
            if(element.type == Element.NOTE) element.color = coloring;
            if(element.type == Element.REST) element.color  = coloring;
            if(element.type == Element.KEYSIG) element.color = coloring;
      }
            
      function colorMeasure(score1, score2, place1, place2, track1, track2, color1, color2) {
            var getMeasure = function(score, place, track) {
                  var cursor = score.newCursor();
                  cursor.track = track;
                  cursor.rewind(Cursor.SCORE_START);
                  if(place==1) return cursor.measure;
                  for(var i=1;i<place; i++) cursor.nextMeasure();
                  return cursor.measure;
            }

            var measure1 = getMeasure(score1, place1, track1);
            var measure2 = getMeasure(score2, place2, track2);
            var segment1 = measure1.firstSegment;
            var segment2 = measure2.firstSegment;
                  
            while(segment1) {
                  var element1 = segment1.elementAt(track1);
                  var element2;
                  if(segment2 == null || testEndBarLine(segment2)) {
                        coloring(element1, color2)
                        segment1 = segment1.nextInMeasure;
                        continue;
                  }
                  else element2 = segment2.elementAt(track2);
                  if(element1 == null) break;
                  if(element1.type == Element.CHORD && element1.type == element2.type) {
                        var different = false;
                        var notes1 = element1.notes;
                        var notes2 = element2.notes;
                        for(var k=0; k< element1.notes.length; k++) {
                              if(notes2[k] == undefined) {
                                    coloring(element1.notes[k], color2)
                                    different = true;
                                    continue;
                              }
                              else coloring(element1.notes[k], color1)
                        }
                        if(different) {
                              segment1 = segment1.nextInMeasure;
                              continue;
                        }
                  }
                  coloring(element1, color1);
                  segment1 = segment1.nextInMeasure;
                  if(segment2 == null) continue;
                  else segment2 = segment2.nextInMeasure;
            }
      }
      
      function colorScores(score1, score2, partsArray) {         
            for(var k= 0; k<partsArray.length; k++) {
                  var staff1 = partsArray[k].oldScore;
                  var staff2 = partsArray[k].newScore;
                  if(staff1 == null) continue;
                  var s1nt, s2nt;
                  if(staff1 !== null) s1nt = (score1.parts[staff1].endTrack-score1.parts[staff1].startTrack)/4;
                  else s1nt = 0;
                  if(staff2 !== null) s2nt = (score2.parts[staff2].endTrack-score2.parts[staff2].startTrack)/4;
                  else s2nt = 0;
                  var s1nm = score1.nmeasures;
                  var s2nm = score2.nmeasures;
                  var s1l = s1nm*s1nt;
                  var s2l = s2nm*s2nt;
                  var m, n;
                  var showModifications = false;
                  
                  if(staff1 !== null) {
                        m = n = 0;
                        var track1 = score1.parts[staff1].startTrack;
                        var track2;
                        if(staff2 !== null) track2 = score2.parts[staff2].startTrack;
                        else track2 = 0;
                        for(var i=1; i<=s1l; i++) {
                              var outcome = 0;
                              for(var j=1; j<=s2l; j++) {
                                    if((j-1)%s2nm==0 && j !== 1) {
                                          n += 1;
                                          track2 += 4;
                                    }
                                    if(partsArray[k].comparisonMatrix[j][i] == 2) {
                                          outcome = 2;
                                          break;
                                    }
                                    if(partsArray[k].comparisonMatrix[j][i] == 1) {
                                          outcome = 1;
                                          break;
                                    }
                              }
                              if(outcome == 2) {
                                    colorMeasure(score1, score2, i-(m*s1nm), j-(n*s2nm), track1, track2, "#03a5fc", "#8a0000")
                                    modificationsModel.append({
                                          s1m: i-(m*s1nm),
                                          s2m: j-(n*s2nm)
                                    })
                                    showModifications = true;
                              }
                              if(outcome == 0) colorMeasure(score1, score1, i-(m*s1nm), i-(m*s1nm), track1, track1, "#8a0000", "#8a0000");
                              n = 0;
                              if(staff2 !== null) track2 = score2.parts[staff2].startTrack;
                              if(i%s1nm==0) {
                                    track1 += 4;
                                    m += 1;;
                              }
                        } 
                  }
                  if(showModifications) {
                        modificationsViewLabel.visible = true;
                        modificationsView.visible = true;
                  }
            }
            
            for(var k= 0; k<partsArray.length; k++) {
                  var staff1 = partsArray[k].newScore;
                  var staff2 = partsArray[k].oldScore;
                  if(staff1 == null) continue;
                  var s1nt, s2nt;
                  if(staff1 !== null) s1nt = (score2.parts[staff1].endTrack-score2.parts[staff1].startTrack)/4;
                  else s1nt = 0;
                  if(staff2 !== null) s2nt = (score1.parts[staff2].endTrack-score1.parts[staff2].startTrack)/4;
                  else s2nt = 0;
                  var s1nm = score2.nmeasures;
                  var s2nm = score1.nmeasures;
                  var s1l = s1nm*s1nt;
                  var s2l = s2nm*s2nt;
                  var m, n;
                  
                  if(staff1 !== null) {
                        m = n = 0;
                        var track1 = score2.parts[staff1].startTrack;
                        var track2;
                        if(staff2 !== null) track2 = score1.parts[staff2].startTrack;
                        else track2 = 0;
                        for(var i=1; i<=s1l; i++) {
                              var outcome = 0;
                              for(var j=1; j<=s2l; j++) {
                                    if((j-1)%s2nm==0 && j !== 1) {
                                          n += 1;
                                          track2 += 4;
                                    }
                                    if(partsArray[k].comparisonMatrix[i][j] == 2) {
                                          outcome = 2;
                                          break;
                                    }
                                    if(partsArray[k].comparisonMatrix[i][j] == 1) {
                                          outcome = 1;
                                          break;
                                    }
                              }
                              if(outcome == 2) colorMeasure(score2, score1, i-(m*s1nm), j-(n*s2nm), track1, track2, "#03a5fc", "#90c900");
                              if(outcome == 0) colorMeasure(score2, score2, i-(m*s1nm), i-(m*s1nm), track1, track1, "#90c900", "#90c900");
                              n = 0;
                              if(staff2 !== null) track2 = score1.parts[staff2].startTrack;
                              if(i%s1nm==0) {
                                    track1 += 4;
                                    m += 1;;
                              }
                        }
                  }
            }
            
      }
      
      function compareScores(score1, score2) {          
            var partsArray = new Array();
            
            initializeParts(score1, score2, partsArray);

            initializeComparisonMatrix(score1, score2, partsArray)
            
            colorScores(score1, score2, partsArray);
      }
        
}